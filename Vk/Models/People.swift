import Foundation
import ObjectMapper

struct CharacterKeys {
	static let name 			= "name"
	static let height 		= "height"
	static let mass 			= "mass"
	static let hairColor	= "hair_color"
	static let skinColor 	= "skin_color"
	static let eyeColor 	= "eye_color"
	static let birthYear 	= "birth_year"
	static let gender 		= "gender"
}


class Character: Mappable {
	
	var name: String?
	var height: String?
	var mass: String?
	var hairColor: String?
	var skinColor: String?
	var eyeColor: String?
	var birthYear: String?
	var gender: String?
	
	required init?(map: Map) {
		
	}
	
	// Mappable
	func mapping(map: Map) {
		name 			<- map[CharacterKeys.name]
		height 		<- map[CharacterKeys.height]
		mass 			<- map[CharacterKeys.mass]
		hairColor <- map[CharacterKeys.hairColor]
		skinColor <- map[CharacterKeys.skinColor]
		eyeColor 	<- map[CharacterKeys.eyeColor]
		birthYear <- map[CharacterKeys.birthYear]
		gender 		<- map[CharacterKeys.gender]
	}
	
}


//"name": "Luke Skywalker",
//"height": "172",
//"mass": "77",
//"hair_color": "blond",
//"skin_color": "fair",
//"eye_color": "blue",
//"birth_year": "19BBY",
//"gender": "male",
//"homeworld": "https://swapi.co/api/planets/1/",
//"films": [
//"https://swapi.co/api/films/2/",
//"https://swapi.co/api/films/6/",
//"https://swapi.co/api/films/3/",
//"https://swapi.co/api/films/1/",
//"https://swapi.co/api/films/7/"
//],
//"species": [
//"https://swapi.co/api/species/1/"
//],
//"vehicles": [
//"https://swapi.co/api/vehicles/14/",
//"https://swapi.co/api/vehicles/30/"
//],
//"starships": [
//"https://swapi.co/api/starships/12/",
//"https://swapi.co/api/starships/22/"
//],
//"created": "2014-12-09T13:50:51.644000Z",
//"edited": "2014-12-20T21:17:56.891000Z",
//"url": "https://swapi.co/api/people/1/"

