import UIKit

class GeneralTableViewController: UITableViewController {

	var peoples: [Character] = []

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return peoples.count
    }

	
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "name", for: indexPath)

        cell.textLabel?.text = peoples[indexPath.row].name

        return cell
    }
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		if segue.identifier == "CharacterVC" {
			if let destVC = segue.destination as? CharacterViewController {
				let nameOfCharacter = (sender as? UITableViewCell)!.textLabel?.text
				destVC.title = nameOfCharacter
				destVC.character = peoples.first { character in
					return nameOfCharacter == character.name
				}
			}
		}
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		let cell = tableView.cellForRow(at: indexPath)
		performSegue(withIdentifier: "CharacterVC", sender: cell)
	}

}
