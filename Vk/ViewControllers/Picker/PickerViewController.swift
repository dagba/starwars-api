import UIKit

class PickerViewController : UIViewController {
	// MARK: - Declarations
	
	var peoples: [Character] = []

	// MARK: - UI Connections
	
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var lastView: UIView!
	@IBOutlet weak var spinner: UIActivityIndicatorView!
	
	// MARK: - Lifecycle and Definitions
	
	override func viewDidLoad() {
		super.viewDidLoad()
		

		scrollView.contentSize = CGSize(width: view.frame.size.width,
		                                height: lastView.frame.origin.y + 64 + 64)
		ApiController.shared.fetchPeople { [weak self] (response,error) in
			self?.peoples = response ?? []
			self?.spinner?.stopAnimating()
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "GeneralTable" {
			guard let title = (sender as? UIButton)?.currentTitle
				else { return }
			
			switch title {
				
			case "People":
				if let tableVC = segue.destination as? GeneralTableViewController {
					tableVC.peoples = peoples
					tableVC.title = "Peoples"
				}
				
			default: break
			}
			
		}
	}

}
