import UIKit

@IBDesignable
class CharacterView: UIView {
	
	
	@IBOutlet weak var massLabel: UILabel!
	@IBOutlet weak var heightLabel: UILabel!
	@IBOutlet weak var genderLabel: UILabel!
	@IBOutlet weak var birthYear: UILabel!
	var printHair = true
	@IBInspectable
	var eyeColor: UIColor = .red {
		didSet {
			setNeedsDisplay()
		}
	}
	@IBInspectable
	var height: CGFloat = 170 {
		didSet {
			heightLabel.text = String(Int(height))
			setNeedsDisplay()
		}
	}
	@IBInspectable
	var skinColor: UIColor = .green {
		didSet {
			setNeedsDisplay()
		}
	}
	@IBInspectable
	var hairColor: UIColor = .black {
		didSet {
			setNeedsDisplay()
		}
	}
	
	override func draw(_ rect: CGRect) {
		
		drawSkin()
		printHair ? drawHair() : print("hair n/a")
		drawEyes()
		drawHeight()
		
	}
	
	func drawEyes() {
		
		var rect = CGRect(x: center.x - 50 - 50, y: center.y - 180, width: 50, height: 50)
		var path = UIBezierPath(ovalIn: rect)
		eyeColor.setFill()
		path.fill()
		rect = CGRect(x: center.x + 50, y: center.y - 180, width: 50, height: 50)
		path = UIBezierPath(ovalIn: rect)
		path.fill()
		path.close()
	}
	
	func drawHeight() {
		
		let path = UIBezierPath()
		path.move(to: CGPoint(x: center.x - 50 - 50 - 50, y: center.y - 180 - 10))
		path.addLine(to: CGPoint(x: center.x - 50 - 50 - 50 - 10, y: center.y - 180 - 10))
		path.move(to: CGPoint(x: center.x - 50 - 50 - 50, y: center.y - 180 - 10))
		path.addLine(to: CGPoint(x: center.x - 50 - 50 - 50, y: center.y + height + 10))
		path.addLine(to: CGPoint(x: center.x - 50 - 50 - 50 - 10, y: center.y + height + 10))
		UIColor.black.setStroke()
		path.lineWidth = 3
		path.stroke()
		path.close()
		
	}
	
	func drawHair() {
		let length = center.y + 170 - center.y - 180 - 20
		let width = (center.x + 50 + 10) - (center.x - 50 - 50 - 10) + 50
		let rect = CGRect(origin: CGPoint(x: center.x - 50 - 50 - 10,y: center.y - 180 - 20 - 5),
		                  size: CGSize(width: width, height: length))
		let path = UIBezierPath(roundedRect: rect, cornerRadius: 5)
		hairColor.setFill()
		path.fill()
		path.close()
	}
	
	func drawSkin() {
		let length = (center.y + height) - (center.y - 180 - 20)
		let width = (center.x + 50 + 10) - (center.x - 50 - 50 - 10) + 50
		let rect = CGRect(origin: CGPoint(x: center.x - 50 - 50 - 10,y: center.y - 180 - 15),
		                  size: CGSize(width: width, height: length))
		let path = UIBezierPath(roundedRect: rect, cornerRadius: 5)
		skinColor.setFill()
		path.fill()
		path.close()
	}
}
