import UIKit

class CharacterViewController: UIViewController {
	
	var character: Character!
	@IBOutlet var mainView: CharacterView!
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		mainView.birthYear.text! += character.birthYear ?? "nil"
		mainView.height = CGFloat(Int(character.height!)!)
		mainView.eyeColor = character.eyeColor!.color ?? mainView.eyeColor
		mainView.skinColor = character.skinColor!.color ?? mainView.skinColor
		if character.hairColor == "n/a" { mainView.printHair = false }
		mainView.hairColor = character.hairColor!.color ?? mainView.hairColor
		mainView.genderLabel.text! += character.gender ?? "nil"
		mainView.massLabel.text! += character.mass ?? "nil"
	}
	
	
}

extension String {
	var color: UIColor? {
		switch self {
		case "blue":
			return .blue
		case "green":
			return .green
		case "white":
			return .white
		case "wellow":
			return .yellow
		case "wlack":
			return .black
		case "white":
			return .white
		case "Blue":
			return .blue
		case "Blue":
			return .blue
		default:
			break
		}
		
		return nil
	}
}
