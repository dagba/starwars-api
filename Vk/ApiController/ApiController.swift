import Foundation
import Alamofire
import ObjectMapper

let baseURL = "https://swapi.co/api/"
struct Resources {
	static let people = "people"
	static let films = "films"
	static let starShips = "starships"
	static let vehicles = "vehicles"
	static let species = "species"
	static let planets = "planets"
}
struct Keys {
	static let results = "results"
}
public typealias bc<T> = (_ result: T?, _ error: Error?) -> Void


class ApiController {
	static let shared = ApiController()
	private init() {
		
	}
	
	func get(resource: String,
	         cmpHandler: @escaping (DataResponse<Any>) -> Void)
	{
		
		Alamofire.request(baseURL + resource).responseJSON { response in
			 cmpHandler(response)
		}
	}
	
	func fetchPeople(completion: @escaping bc<[Character]>) {
		get(resource: Resources.people) { response in
			switch response.result {
			case .success:
				print("Validation Successful")
				
//				print(response.result.value)
				
				guard let data = (response.result.value as! [String:Any])[Keys.results] else {return}
				let array = Mapper<Character>().mapArray(JSONObject: data)
				
				completion(array,nil)

			case .failure(let error):
				print(error)
				completion(nil, error)
			}
			
			
			
		}
		
	}
	
}

